## Please read the instructions carefully

1. Comments are in a yellow sticky paper.
2. Please note the colors carefully. Match the font and background colors to the best of your ability.
3. Font sizes are also important. Match the image as much as possible.
4. Determine heading levels (h1, h2, h3, h4) based on the content and its location.
5. Notice the background colors for various sections of the page, and notice that they are full width.

Best of luck!

---

![Final Exam](https://gitlab.com/abijeet.p/final-exam-html-css/raw/master/batch9_exam.png)

